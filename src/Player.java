public class Player {
    private String name;
    private int result;
    private String sign;

    Player(String name, String sign){
        this.name = name;
        this.result = 0;
        this.sign = sign;
    }

    public void riseResult() {
        this.result++;
    }

    public void eraseResult() {
        this.result=0;
    }

    public String getName() {
        return name;
    }

    public int getResult() {
        return result;
    }
}
