import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class XO_BoardController {
    @FXML
    private ImageView field00;
    @FXML
    private ImageView field01;
    @FXML
    private ImageView field02;
    @FXML
    private ImageView field10;
    @FXML
    private ImageView field11;
    @FXML
    private ImageView field12;
    @FXML
    private ImageView field20;
    @FXML
    private ImageView field21;
    @FXML
    private ImageView field22;
    @FXML
    private Button start;
    @FXML
    private Button reset;
    @FXML
    private Button goOn;
    @FXML
    private Button endGame;

    @FXML
    private TextArea scorePlayerOne;
    @FXML
    private TextArea scorePlayerTwo;
    @FXML
    private ImageView whoIsNowMoving;

    private final Image imageOcolor;
    private final Image imageXcolor;

    XO_BoardController(){
        imageOcolor = new Image("O kolor.png");
        imageXcolor = new Image("X kolor.png");
    }


    @FXML
    public void setField00(){

        setField(field00);
        toggleImageViewWhoIsNowMovingImage();

    }

    @FXML
    public void setField01(){

        setField(field01);
        toggleImageViewWhoIsNowMovingImage();

    }

    @FXML
    public void setField02(){

        setField(field02);
        toggleImageViewWhoIsNowMovingImage();

    }

    @FXML
    public void setField10(){

        setField(field10);
        toggleImageViewWhoIsNowMovingImage();

    }

    @FXML
    public void setField11(){

        setField(field11);
        toggleImageViewWhoIsNowMovingImage();

    }

    @FXML
    public void setField12(){

        setField(field12);
        toggleImageViewWhoIsNowMovingImage();

    }
    @FXML
    public void setField20(){

        setField(field20);
        toggleImageViewWhoIsNowMovingImage();

    }

    @FXML
    public void setField21(){

        setField(field21);
        toggleImageViewWhoIsNowMovingImage();

    }

    @FXML
    public void setField22(){

        setField(field22);
        toggleImageViewWhoIsNowMovingImage();

    }




    private void toggleImageViewWhoIsNowMovingImage() {
        if(whoIsNowMoving.getImage().equals(imageOcolor)){
            whoIsNowMoving.setImage(imageXcolor);
        }else{
            whoIsNowMoving.setImage(imageOcolor);
        }
    }

    private void setField(ImageView imageViewField) {
        if(whoIsNowMoving.getImage().equals(imageOcolor)) {
            imageViewField.setImage(imageOcolor);
        } else {
           imageViewField.setImage(imageXcolor);
        }
        imageViewField.setDisable(true);
    }

}
