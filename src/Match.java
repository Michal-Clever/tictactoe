public class Match {
    private Player firstPlayer;
    private Player secondPlayer;
    private int roundLimit;
    private int roundPlayed;

    Match(Player first, Player second){
        roundLimit = 6;
        roundPlayed = 0;
        this.firstPlayer=first;
        this.secondPlayer =second;
    }

    void riseFirstPlayerResult(){
        firstPlayer.riseResult();
    }

    void riseSEcondPlayerResult(){
        secondPlayer.riseResult();
    }

    void resetGame(){
        firstPlayer.eraseResult();
        secondPlayer.eraseResult();
    }

    public boolean isMatchFinished(){
        if (roundPlayed>=roundLimit){
            return true;
        }else if(roundLimit - roundPlayed < Math.abs(firstPlayer.getResult() - secondPlayer.getResult())){
            return true;
        } else {
            return false;
        }
    }




}
